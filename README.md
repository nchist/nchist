# NicoCache history

NicoCacheとその派生の歴史を記録したリポジトリです。
記録用であり、パッチは受け付けていません。


nl_mod_modブランチが現在も更新されており、他の更新は停止しています。

## 記録に含まれていないもの

### 本家

- 2007/09/26 NicoCache 0.03a
- 2007/10/08 NicoCache 0.04
- 2007/10/10 NicoCache 0.05
- 2007/10/11 NicoCache 0.06
- 2007/10/14 NicoCache 0.07
- 2007/10/15 NicoCache 0.08
- 2007/10/17 NicoCache 0.10
- 2007/10/24 NicoCache 0.20
- 2007/10/30 NicoCache 0.23
- 2007/11/02 NicoCache 0.25

### NicoCache_p

- ?
- 2007/10/15 0.08p
- 2007/10/16 0.09p
- 2007/10/17 0.10p
- 2007/10/19 0.11p1
- 2007/10/19 0.12p
- 2007/10/24 0.12p1
- 2007/10/29 0.21p- alpha
- 2007/10/29 0.22p- alpha
- 2007/10/31 0.23p- beta
- 2007/10/31 0.24p- beta
- 2007/11/03 0.25p- beta
- 2007/11/04 0.26p
- 2007/11/04 0.27p
- 2007/11/11 0.27p1
- 2007/11/11 0.27p2
- 2007/11/14 0.27p3
- 2007/11/17 0.28p
- 2007/11/21 0.28p1
- 2007/11/23 0.29p
- 2007/11/28 0.31p
- 2008/03/05 0.31p1
- 2008/03/06 0.31p1
- 2008/03/06 0.32p
- 2008/05/25 0.35p
- 2009/03/23 0.38p
- 2009/03/25 0.39p
- 2009/08/03 0.42p

### NicoCache_nl

- NicoCache_nl 2007/10/12 rev.2 (23:10リリース)
- NicoCache_nl 2007/10/13
- NicoCache_nl 2007/10/13 NGWORDテスト版
- NicoCache_nl 2007/10/14
- NicoCache_nl 2007/10/14 rev.2
- NicoCache_nl 2007/10/14 rev.3
- NicoCache_nl 2007/10/16
- NicoCache_nl 2007/10/16 rev.2
- NicoCache_nl 2007/10/23
- NicoCache_nl 2007/10/23 rev.2
- NicoCache_nl 2007/10/24
- NicoCache_nl 2007/10/24 rev.2
- NicoCache_nl 2007/10/25
- NicoCache_nl beta 1 (2007/10/26)
- NicoCache_nl beta 2 (2007/10/26)
- NicoCache_nl beta 3 (2007/10/26)
- NicoCache_nl beta 4 (2007/10/26)
- NicoCache_nl beta 5 (2007/10/27)
- NicoCache_nl beta 6 (2007/10/27)
- NicoCache_nl beta 7 (2007/10/27)
- NicoCache_nl beta 8 (2007/10/27)
- NicoCache_nl beta 9 (2007/10/29)
- NicoCache_nl beta 10 (2007/10/30)
- NicoCache_nl beta 11 (2007/10/30)
- NicoCache_nl beta 12 (2007/10/30)
- NicoCache_nl beta 13 (2007/10/31)
- NicoCache_nl beta 14 (2007/11/01)
- NicoCache_nl beta 15 (2007/11/01)
- NicoCache_nl beta 16 (2007/11/02)
- NicoCache_nl beta 17 (2007/11/03)
- NicoCache_nl beta 18 (2007/11/04)
- NicoCache_nl beta 19 (2007/11/08)
- NicoCache_nl beta 20 (2007/11/10)
- NicoCache_nl beta 21 (2007/11/19)
- NicoCache_nl beta 22 (2007/11/20)
- NicoCache_nl beta 23 (2007/11/22)
- NicoCache_nl beta 24 (2007/11/23)
- NicoCache_nl beta 25 (2007/11/24)
- NicoCache_nl beta 26 (2007/11/24)
- NicoCache_nl beta 27 (2007/11/24)
- NicoCache_nl beta 28 (2007/11/26)
- NicoCache_nl beta 29 (2007/11/26)
- NicoCache_nl beta 30 (2007/11/26)
- NicoCache_nl beta 31 (2007/11/28)
- NicoCache_nl beta 32 (2007/12/14)
- NicoCache_nl beta 33 (2007/12/16)
- NicoCache_nl beta 34 (2007/12/27)
- NicoCache_nl rc1 (2008/02/09)
- NicoCache_nl rc2.00 (2008/02/12)
- NicoCache_nl rc2.01 (2008/02/12)
- NicoCache_nl RC2.02 (2008/02/12)
- NicoCache_nl RC2.03 (2008/02/12)
- NicoCache_nl RC2.04 (2008/02/16)
- NicoCache_nl SP1.00 (2008/03/05)
- NicoCache_nl SP1.00a (2008/03/05)
- NicoCache_nl SP1.01 (2008/03/06) ※本家0.31ベース
- NicoCache_nl SP1.02 (2008/03/06)
- NicoCache_nl SP1.03 (2008/03/06)
- NicoCache_nl SP1.04 (2008/03/08)
- NicoCache_nl SP1.05 (2008/03/08)
- NicoCache_nl SP1.06 (2008/03/08)
- NicoCache_nl SP1.07 (2008/03/11)
- NicoCache_nl SP1.08 (2008/04/19)
- NicoCache_nl SP1.09 (2008/05/06)
- NicoCache_nl SP1.10 (2008/05/25)
- NicoCache_nl SP1.11 (2008/06/27)
- NicoCache_nl SP1.12 (2008/06/28)
- NicoCache_nl SP1.13 (2008/06/29)
- NicoCache_nl 夏.01 (2008/07/05)
- NicoCache_nl 夏.02 (2008/07/06)
- NicoCache_nl 夏.03 (2008/07/13)
- NicoCache_nl 夏.04 (2008/07/14)
- NicoCache_nl 夏.05 (2008/07/25)
- NicoCache_nl 夏.06 (2008/07/26)

### NicoCache_nl (9) NicoCache_nl(9)+mod  NicoCache_nl(9)+mod+mod

- 2010/06/25 20:26 [nl448.zip]	NicoCache_nl (9).10テスト版 キャッシュ完了Extensionとか
- 2010/07/06 23:51 [nl464.zip]	NicoCache_nl (9).10a バグ修正
- 2010/07/08 20:18 [nl470.zip]	NicoCache_nl (9).10a +100708mod getthumbinfoキャッシュとか(本体のみ)
- 2010/07/15 22:35 [nl493.zip]	NicoCache_nl (9).10a +100715mod 細かな修正とか(本体のみ)
- 2010/07/21 20:02 [nl512.zip]	NicoCache_nl (9).11 新新プレ対応＆バグ修正
- 2010/07/22 01:24 [nl514.zip]	NicoCache_nl localフォルダからの取得エラー表示版(本体のみ)
- 2010/07/22 21:56 [nl519.zip]	NicoCache_nl ローカルからプレイヤー取得 テスト版(本体のみ)
- 2010/07/23 19:05 [nl521.zip]	swfConvertをストリーム処理するテスト(本体のみ)
- 2010/08/02 00:20 [nl552.zip]	NicoCache_nl (9).11 +100801mod swfConvertのストリーム処理とか
- 2010/08/05 22:32 [nl560.zip]	NicoCache_nl (9).11 +100805mod 不具合修正とか(テスト版・本体のみ)
- 2010/08/07 12:36 [nl566.zip]	NicoCache_nl (9).11 +100807mod 不具合修正(テスト版・本体のみ)
- 2010/08/11 19:26 [nl571.zip]	NicoCache_nl (9).12 スレの修正まとめとか
- 2010/08/20 22:41 [nl582.zip]	NicoCache_nl (9).12 +100820mod(テスト版・本体のみ)
- 2010/09/03 23:51 [nl590.zip]	NicoCache_nl (9).12 +100903mod 不具合修正(テスト版・本体のみ)
- 2010/09/14 21:01 [nl603.zip]	NicoCache_nl (9).12 +100914_2mod SWFヘッダ処理とか(テスト版・本体のみ)
- 2010/09/18 23:52 [nl610.zip]	NicoCache_nl (9).12 +100918mod 不具合修正(テスト版・本体のみ)
- 2010/09/26 19:17 [nl613.zip]	NicoCache_nl (9).12 +100926mod 不具合修正とか(テスト版・本体のみ)
- 2010/10/11 23:24 [nl618.zip]	NicoCache_nl (9).12 +101011mod　threadkey付きコメ取得対応(本体のみ)
- 2010/10/17 10:00 [nl629.zip]	NicoCache_nl+101017mod　タイトル取得修正とか(更新分のみ)
- 2010/10/29 20:36 [nl642.zip]	NicoCache_nl (9).12 +101029mod　原宿にとりあえず対応(更新分のみ)
- 2010/11/03 22:57 [nl652.zip]	NicoCache_nl (9).12 +101103mod　フィルタ見直しとか(テスト版・更新分のみ)
- 2010/12/19 23:28 [nl662.zip]	NicoCache_nl+101219mod 単体パッケージ版 (本家NicoCache 0.45をマージ)
- 2010/12/21 23:17 [nl662.zip]	NicoCache_nl+101221mod 単体パッケージ版(変更点多数) 不具合あり
- 2011/01/13 19:54 [nl678.zip]	NicoCache_nl+110113mod 機能追加とか不具合修正とか
- 2011/01/18 22:02 [nl681.zip]	NicoCache_nl+110118mod GUI起動対応と不具合修正
- 2011/01/22 21:15 [nl683.zip]	NicoCache_nl+110122mod ログウィンドウのタブ化とか
- 2011/01/25 23:43 [nl685.zip]	NicoCache_nl+110125mod 拡張ロガーの追加とか
- 2011/02/19 23:42 [nl693.zip]	NicoCache_nl+110219mod ログウィンドウ周りの修正とか
- 2011/04/11 23:36 [nl705.zip]	NicoCache_nl+110411mod テスト版(差分のみ・fetcher同梱)
- 2011/04/24 21:10 [nl709.zip]	NicoCache_nl+110424mod 不具合修正とか
- 2011/05/22 21:42 [nl719.zip]	NicoCache_nl+110522mod テスト版
- 2011/06/04 23:47 [nl721.zip]	NicoCache_nl+110604mod $TS追加とかフィルタ修正とか
- 2011/07/06 23:59 [nl723.zip]	NicoCache_nl+110706mod JNI対応とか(9)対応削除とか
- 2011/11/23 23:56 [nl765.zip]	NicoCache_nl+111123mod
- 2011/11/24 21:36 [nl765.zip]	NicoCache_nl+111124mod
- 2014/05/09 22:11 [up255.zip]	nl+mod- patch140509 (ソース差分のみ)
- 2014/09/08 08:59 [up270.zip]	nl+mod- patch140908 (フィルタ&ソース差分)
- 2014/09/14 07:52 [up271.zip]	nl+mod- patch140914 (フィルタ&ソース差分)
- 2015/03/02 20:57 [up291.zip]	nl+mod- patch150302 (フィルタ&ソース累積差分)
- 2016/12/24 01:51 [up319.zip]	NicoCache_nl+150304mod+161224mod HTML5 (up292との差分)
- 2016/12/24 01:52 [up320.zip]	dmc- 20161224 extension
- 2017/01/02 04:23 [up323.zip]	dmc- 170102
- 2017/01/08 15:59 [up329]	NicoCache_nl+150304mod+170108mod HTML5+dmc 人柱版
- 2017/01/11 01:17 [up335]	NicoCache_nl+150304mod+170111mod
- 2017/01/14 12:49 [up338]	NicoCache_nl+150304mod+170114mod
- 2017/01/15 15:07 [up342]	NicoCache_nl+150304mod+170115mod2
- 2017/01/20 03:15 [up348]	NicoCache_nl+150304mod+170120mod
- 2017/02/02 23:12 [up357]	NicoCache_nl+150304mod+170202mod
- 2017/02/07 04:05 [up360]	NicoCache_nl+150304mod+170207mod
- 2017/02/11 00:47 [up366]	NicoCache_nl+150304mod+170210mod
- 2017/04/25 21:52 [up429.7z]	NicoCache_nl+150304mod+170425mod+tls 人柱版
- 2017/05/03 06:17 [up432.7z]	NicoCache_nl+150304mod+170503mod
- 2017/05/03 06:17 [up434.7z]	NicoCache_nl+150304mod+170503mod+tls (人柱版)
- 2017/05/03 07:25 [up437.7z]	NicoCache_nl+150304mod+170503mod2+tls (人柱版)
- 2017/05/04 06:19 [up442.7z]	NicoCache_nl+150304mod+170504mod+tls (人柱版)
- 2018/04/06 01:07 [nc0138.7z]	NicoCache_nl+150304mod+180405mod HTML5+dmc
- 2018/06/01 19:14 [nc0175.7z]	NicoCache_nl+150304mod+180601mod HTML5+dmc
- 2019/01/08 06:53 [nc0307.7z]	NicoCache_nl+150304mod+190108mod (く) + alertOldBinary Extension v180630
- 2019/01/23 00:45 [nc0317.7z]	NicoCache_nl+150304mod+190123mod (く) + alertOldBinary Extension v180630
- 2019/01/25 18:30 [nc0324.7z]	NicoCache_nl+150304mod+190125mod (く) + alertOldBinary Extension v180630
- 2019/05/22 03:48 [nc0384.7z]	NicoCache_nl+150304mod+190522mod (く) + alertOldBinary Extension v180630

